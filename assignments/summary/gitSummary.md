**Git Summary:**

1. At this point, we can do all the basic local Git operations — creating or cloning a repository, making changes, staging and committing those changes, and viewing the history of all the changes the repository has been through. Next, we'll cover Git's killer feature: its branching model.
2. Git doesn’t store data as a series of changesets or differences, but instead as a series of snapshots.
3. When we make a commit, Git stores a commit object that contains a pointer to the snapshot of the content you staged. 
4. This object also contains the author’s name and email address, the message that you typed, and pointers to the commit or commits that directly came before this commit (its parent or parents): zero parents for the initial commit, one parent for a normal commit, and multiple parents for a commit that results from a merge of two or more branches.
5. To visualize this, let’s assume that you have a directory containing three files, and you stage them all and commit. 
6. Staging the files computes a checksum for each one , stores that version of the file in the Git repository (Git refers to them as blobs), and adds that checksum to the staging area.
                           ** $ git add README test.rb LICENSE
                              $ git commit -m 'Initial commit'**

7. When we create the commit by running git commit, Git checksums each subdirectory (in this case, just the root project directory) and stores them as a tree object in the Git repository. Git then creates a commit object that has the metadata and a pointer to the root project tree so it can re-create that snapshot when needed.

**Git workflow:**

               - Initialize the central repository
               - Hosted central repository
               - Clone the central repository
               - Make changes and commit
               - Push new commits to central repositories
               - Managing Conflicts



**Git Generally Only Adds Data:**

1. When you do actions in Git, nearly all of them only add data to the Git database. It is hard to get the system to do anything that is not undoable or to make it erase data in any way.
2. As with any VCS, you can lose or mess up changes you haven’t committed yet, but after you commit a snapshot into Git, it is very difficult to lose, especially if you regularly push your database to another repository.
3. This makes using Git a joy because we know we can experiment without the danger of severely screwing things up. For a more in-depth look at how Git stores its data and how you can recover data that seems lost, see Undoing Things.

**Git repository now contains five objects:**

- **Three blobs** (each representing the contents of one of the three files), 
- **One tree** that lists the contents of the directory and specifies which file names are stored as blobs.
- **One commit** with the pointer to that root tree and all the commit metadata.

**Git Terminology:**

**Branch**
A branch is a version of the repository that diverges from the main working project. It is an essential feature available in most modern version control systems. A Git project can have more than one branch. We can perform many operations on Git branch-like rename, list, delete, etc.

**Checkout**
In Git, the term checkout is used for the act of switching between different versions of a target entity. The git checkout command is used to switch between branches in a repository.

**Cherry-Picking**
Cherry-picking in Git is meant to apply some commit from one branch into another branch. In case you made a mistake and committed a change into the wrong branch, but do not want to merge the whole branch. You can revert the commit and cherry-pick it on another branch.

**Clone**
The git clone is a Git command-line utility. It is used to make a copy of the target repository or clone it. If I want a local copy of my repository from GitHub, this tool allows creating a local copy of that repository on your local directory from the repository URL.

**Fetch**
It is used to fetch branches and tags from one or more other repositories, along with the objects necessary to complete their histories. It updates the remote-tracking branches.

**HEAD**
HEAD is the representation of the last commit in the current checkout branch. We can think of the head like a current branch. When you switch branches with git checkout, the HEAD revision changes, and points the new branch.

**Index**
The Git index is a staging area between the working directory and repository. It is used as the index to build up a set of changes that you want to commit together.

**Master**
Master is a naming convention for Git branch. It's a default branch of Git. After cloning a project from a remote server, the resulting local repository contains only a single local branch. This branch is called a "master" branch. It means that "master" is a repository's "default" branch.

**Merge**
Merging is a process to put a forked history back together. The git merge command facilitates you to take the data created by git branch and integrate them into a single branch.

**Origin**
In Git, "origin" is a reference to the remote repository from a project was initially cloned. More precisely, it is used instead of that original repository URL to make referencing much easier.

**Pull/Pull Request**
The term Pull is used to receive data from GitHub. It fetches and merges changes on the remote server to your working directory. The git pull command is used to make a Git pull.

**Push**
The push term refers to upload local repository content to a remote repository. Pushing is an act of transfer commits from your local repository to a remote repository. Pushing is capable of overwriting changes; caution should be taken when pushing.

**Rebase**
In Git, the term rebase is referred to as the process of moving or combining a sequence of commits to a new base commit. Rebasing is very beneficial and visualized the process in the environment of a feature branching workflow.
From a content perception, rebasing is a technique of changing the base of your branch from one commit to another.




