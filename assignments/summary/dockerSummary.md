**Docker Summary**
Docker is an open-source utility that automates the deployment and management of programs inside software containers.Docker has the ability to reduce the size of development by providing a smaller footprint of the operating system via containers.

With containers, it becomes easier for teams across different units, such as development, QA and Operations to work seamlessly across applications.

You can deploy Docker containers anywhere, on any physical and virtual machines and even on the cloud.

Since Docker containers are pretty lightweight, they are very easily scalable.

**Docker Commands:**

command 1:docker attach
description 1:Attach local standard input, output, and error streams to a running container.

command 2:docker build
description 2:Build an image from a Dockerfile.

command 3:docker commit
description 3:Create a new image from a container’s changes.

command 4:docker diff
description:Inspect changes to files or directories on a container’s filesystem.

command 5:Usage: docker rm <container id>
description:This command is used to delete a stopped container.

command 6:docker exec 
description:Runs a command in a run-time container.

command 7:docker search
description:Searches the Docker Hub for images

command 8:docker attach
description:Attaches to a running container

command 9:docker pull
description:Pulls an image or a repository from a registry

command 10:docker push
description:Pushes an image or a repository to a registry

**Docker terminologies:**
1. **Container image:** A package with all the dependencies and information needed to create a container. An image includes all the dependencies (such as frameworks) plus deployment and execution configuration to be used by a container runtime.
2. **Dockerfile:** A text file that contains instructions for building a Docker image. It's like a batch script, the first line states the base image to begin with and then follow the instructions to install required programs, copy files, and so on, until we get the working environment you need.
3. **Build:** The action of building a container image based on the information and context provided by its Dockerfile, plus additional files in the folder where the image is built. 
4. **Container: **An instance of a Docker image. A container represents the execution of a single application, process, or service. It consists of the contents of a Docker image, an execution environment, and a standard set of instructions.
5. **Tag:** A mark or label you can apply to images so that different images or versions of the same image can be identified.
6. **Repository (repo):** A collection of related Docker images, labeled with a tag that indicates the image version. Some repos contain multiple variants of a specific image, such as an image containing SDKs (heavier), an image containing only runtimes (lighter), etc. Those variants can be marked with tags.
7. **Docker Hub:** A public registry to upload images and work with them. Docker Hub provides Docker image hosting, public or private registries, build triggers and web hooks, and integration with GitHub and Bitbucket. 
